package pl.edu.richard.sorting;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        runProgram(args);
    }

    private static void runProgram(String[] args) {
        List<Integer> ints = parseNumbers(args);

        Sorting.sort(ints);

        printNumbers(ints);
    }

    private static List<Integer> parseNumbers(String[] numbers) {
        if (numbers.length > 10) {
            System.out.println("Max 10 numbers allowed!");
            System.exit(1);
        }

        List<Integer> ints = new ArrayList<>(numbers.length);

        try {
            for (String number : numbers) {
                ints.add(Integer.parseInt(number));
            }
        } catch (NumberFormatException e) {
            System.out.println("Only numbers are allowed!");
            System.exit(2);
        }
        return ints;
    }

    private static void printNumbers(List<Integer> ints) {
        for (int integer : ints) {
            System.out.print(integer + " ");
        }
    }
}
