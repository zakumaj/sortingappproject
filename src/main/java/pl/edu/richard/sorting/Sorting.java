package pl.edu.richard.sorting;

import java.util.List;
import java.util.Collections;

/**
 * This is a sorting method class
 */
public class Sorting {

    private Sorting() {}

    /**
     * This is the method that sort the given list of integers.
     * This method modifies elements of the provided list.
     *
     * @param list - The provided list of integers.
     */
    public static void sort(List<Integer> list) {
        Collections.sort(list);
    }
}
