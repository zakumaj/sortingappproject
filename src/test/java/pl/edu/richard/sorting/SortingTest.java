package pl.edu.richard.sorting;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortingTest {

    @Test(expected = NullPointerException.class)
    public void testNullCase() {
        Sorting.sort(null);
    }

    @Test
    public void testEmptyCase() {
        List<Integer> actual = Collections.emptyList();
        List<Integer> expected = Collections.emptyList();
        Sorting.sort(actual);

        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testSingleElementArrayCase() {
        List<Integer> given = Collections.singletonList(1);
        List<Integer> expected = Collections.singletonList(1);
        Sorting.sort(given);
        Assert.assertArrayEquals(given.toArray(), expected.toArray());
    }

    @Test
    public void testSortedArraysCase() {
        List<Integer> given = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> expected = Arrays.asList(1, 2, 3, 4, 5);
        Sorting.sort(given);
        Assert.assertArrayEquals(expected.toArray(), given.toArray());
    }

}

